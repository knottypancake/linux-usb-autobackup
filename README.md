## About

This is a set of files for running a backup script when plugging in a USB disk on Linux. It uses udev, a systemd service, and a shell script. You'll have to customize some parts to get it to work on your system, but if you can do some light shell scripting, it should be easy.

## How it works

There are three files in this package that make the backup work:

1. `00-usb-device.rules` - The udev rules file
2. `autobackup.service` - The systemd service
3. `backup.sh` - The shell script that the service executes

You'll need to customize the udev rules and the backup script (see the Customization section).

When a USB device with specific vendor and product IDs is plugged in, the udev rules will match and start the autobackup service. The autobackup service executes the backup script. This might seem like a strange way to do it, but it nicely solves a major problem with executing scripts from udev: scripts executed from udev are only allowed to run for a few seconds before they are killed. By using systemd, we work around that problem.

## Customization

Before you do the install, you'll need to customize a few things.

First, the `00-usb-device.rules` file has a hardcoded device and vendor and product IDs which should be changed for different devices.

To find the right device, plug in your backup drive and check what device it is assigned with `lsblk` or `mount`. It will probably be something like `sdb` or `sdc`. Replace `<<device>>` with this value in `00-usb-device.rules`. If your device name is not consistent on your system, I think there are ways to work around this with udev, but I don't know them.

You can get the vendor and product IDs by running the `lsusb` command when your device is plugged in:

```
$ lsusb
...
Bus 001 Device 014: ID 0a16:9903 Trek Technology (S) PTE, Ltd Mass storage
...
```

In this case, the vendor ID is `0a16` and the product ID is `9903`. Replace `<<vendorId>>` and `<<productId>>` with these values.

Next, you'll need to customize the `backup.sh` script. It is documented and if you can do some light shell scripting, you can probably figure out what you need to change.

## Installation

The Makefile will install the udev rules file in `/etc/udev/rules.d`, the autobackup service file in `/etc/systemd/system` and the backup script in `/usr/local/sbin`.

```
$ sudo make install
```

And you may need to reload udev:

```
$ sudo udevadm control --reload
```

Now, the backup script should be called whenever you plug in your device!
