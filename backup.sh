#!/bin/bash

# Give a few seconds for the drive to mount if necessary
sleep 3

# Define your source and target directories
SOURCE_DIR=/home/me
TARGET_DIR=/mnt/backup

# Uncomment this if you need to mount the drive first. This assumes that you've
# defined your mount point in /etc/fstab. Otherwise, you'll need to expand this
# command to include the device.
#/usr/bin/mount ${TARGET_DIR}

# Write a log message so we can see from the logs that this has started
logger "Auto backup device mounted"

# If you want to send yourself an email when the backup starts, uncomment this
# line and add your email address. You'll need a working `mail` command.
#echo "Starting backup at $(date +%H:%M:%S)" | mail -s "Backup started on $(hostname)" you@example.com

# Do the backup with rsync or whatever you want. You'll probably want to
# customize this for your system
/usr/bin/rsync -a --delete "${SOURCE_DIR}/" "${TARGET_DIR}"

sleep 3

# Unmount the device
/usr/bin/umount ${TARGET_DIR}

# Write a log message so we can see from the logs that this has finished
logger "Auto backup device unmounted"

# If you want to send yourself an email when the backup finishes, uncomment this
# line and add your email address. You'll need a working `mail` command.
#echo "Finished backup at $(date +%H:%M:%S)" | mail -s "Backup finished on $(hostname)" you@example.com
